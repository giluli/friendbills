﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Domain.Services
{
    public class PersonService : GenericService<Person>, IPersonService
    {
        private readonly IPersonRepository _personRepository;

        public PersonService(IPersonRepository personRepository) : base(personRepository)
        {
            _personRepository = personRepository;
        }

        public IEnumerable<Person> FindByName(string name)
        {
            return _personRepository.FindByName(name);
        }

        public IEnumerable<Person> GetActivePersons()
        {
            return _personRepository.GetAll().Where(p => p.IsActive.Equals(true));
        }

        public string GetFullName(Person p)
        {
            return p.FullName;
        }

        public void DesactivePerson(long id)
        {
            _personRepository.DesactivePerson(id);
        }
    }
}
