﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Domain.Services
{
    public class PersonGroupService : GenericService<PersonGroup>, IPersonGroupService
    {
        private readonly IPersonGroupRepository _persongroupRepository;

        public PersonGroupService(IPersonGroupRepository personGroupRepository) : base(personGroupRepository)
        {
            _persongroupRepository = personGroupRepository;
        }

        public IEnumerable<PersonGroup> GetPersonsByGroup(long id)
        {
            return _persongroupRepository.GetAll().Where(p => p.FriendGroupId.Equals(id));
        }
    }
}
