﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Domain.Services
{
    public class BillService : GenericService<Bill>, IBillService
    {
        private readonly IBillRepository _billRepository;

        public BillService(IBillRepository billRepository) : base(billRepository)
        {
            _billRepository = billRepository;
        }

        public IEnumerable<Bill> FindByDescription(string description)
        {
            return _billRepository.FindByDescription(description);
        }

        public IEnumerable<Bill> GetPaidBills()
        {
            return _billRepository.GetAll().Where(b => b.IsPaid.Equals(true));
        }

        public IEnumerable<Bill> GetNotPaidBills()
        {
            return _billRepository.GetAll().Where(b => b.IsPaid.Equals(false));
        }

        public Bill GetByIdNoTracking(long id)
        {
            return _billRepository.GetByIdNoTracking(id);
        }

    }
}
