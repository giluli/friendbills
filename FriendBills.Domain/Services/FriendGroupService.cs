﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Domain.Services
{
    public class FriendGroupService : GenericService<FriendGroup>, IFriendGroupService
    {
        private readonly IFriendGroupRepository _groupRepository;

        public FriendGroupService(IFriendGroupRepository groupRepository) : base(groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public IEnumerable<FriendGroup> FindByName(string name)
        {
            return _groupRepository.FindByName(name);
        }

        public IEnumerable<FriendGroup> FindGroupsByPerson(long id)
        {
            return _groupRepository.GetAll().Where(g => g.PersonId.Equals(id));
        }
    }
}
