﻿using System.Collections.Generic;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using System.Linq;
using System;

namespace FriendBills.Domain.Services
{
    public class PaymentService : GenericService<Payment>, IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(IPaymentRepository paymentRepository) : base(paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public IEnumerable<Payment> GetPaymentsByBill(long id)
        {
            return _paymentRepository.GetAll().Where(p => p.BillId.Equals(id));
        }

        public Bill UpdateBillSubtotal(Payment payment, Bill billToUpdate)
        {
            return billToUpdate.UpdateBillSubtotal(payment, billToUpdate);
        }

        public Bill UpdadeIfPaid(Payment payment, Bill billToUpdate)
        {
            return billToUpdate.UpdadeIfPaid(payment, billToUpdate);
        }
    }
}
