﻿using System;

namespace FriendBills.Domain.Entities
{
    public class Payment
    {
        public long PaymentId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public long BillId { get; set; }
        public virtual Bill Bill { get; set; }

        public long PersonId { get; set; }
        public virtual Person Person { get; set; } 
    }
}
