﻿using System;
using System.Collections.Generic;

namespace FriendBills.Domain.Entities
{
    public class Person
    {
        public long PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string NickName { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Bill> Bills{ get; set; }
        public virtual ICollection<FriendGroup> Groups { get; set; }
        public virtual ICollection<PersonGroup> PersonsGroups { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }

        public string FullName
        {
            get
            {
                return LastName + ", " + FirstName;
            }
        }
    }
}
