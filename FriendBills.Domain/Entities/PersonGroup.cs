﻿namespace FriendBills.Domain.Entities
{
    public class PersonGroup
    {
        public long PersonGroupId { get; set; }

        public long FriendGroupId { get; set; }
        public virtual FriendGroup FriendGroup { get; set; }

        public long PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
