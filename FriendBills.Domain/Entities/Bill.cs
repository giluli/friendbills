﻿using System;
using System.Collections.Generic;

namespace FriendBills.Domain.Entities
{
    public class Bill
    {
        public long BillId { get; set; }
        public string Description{ get; set; }
        public DateTime Date { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public bool IsPaid { get; set; }

        public long PersonId { get; set; }
        public virtual Person Person { get; set; }

        public long FriendGroupId { get; set; }
        public virtual FriendGroup FriendGroup { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }

        public Bill UpdateBillSubtotal(Payment payment, Bill billToUpdate)
        {
            billToUpdate.SubTotal = billToUpdate.SubTotal + payment.Amount;
            return billToUpdate;
        }

        public Bill UpdadeIfPaid(Payment payment, Bill billToUpdate)
        {
            int valor = Decimal.Compare(payment.Amount, billToUpdate.Total);
            if (valor >= 0)
            {
                billToUpdate.IsPaid = true;
            }
            return billToUpdate;
        }
    }
}
