﻿using System.Collections.Generic;

namespace FriendBills.Domain.Entities
{
    public class FriendGroup
    {
        public long FriendGroupId { get; set; }
        public string Name { get; set; }

        public long PersonId { get; set; }
        public virtual Person Person { get; set; }

        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<PersonGroup> PersonsGroups { get; set; }
    }
}
