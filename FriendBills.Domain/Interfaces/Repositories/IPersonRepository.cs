﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        IEnumerable<Person> FindByName(string name);
        void DesactivePerson(long id);
    }
}