﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IFriendGroupRepository : IGenericRepository<FriendGroup>
    {
        IEnumerable<FriendGroup> FindByName(string name);
    }
}
