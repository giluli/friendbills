﻿using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(long id);

        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(long id);
        void Dispose();
    }
}
