﻿using FriendBills.Domain.Entities;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IPersonGroupRepository : IGenericRepository<PersonGroup>
    {
    }
}