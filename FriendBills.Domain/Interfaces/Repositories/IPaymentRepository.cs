﻿using FriendBills.Domain.Entities;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IPaymentRepository : IGenericRepository<Payment>
    {
    }
}
