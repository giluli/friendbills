﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Repositories
{
    public interface IBillRepository : IGenericRepository<Bill>
    {
        IEnumerable<Bill> FindByDescription(string description);
        Bill GetByIdNoTracking(long id);
    }
}
