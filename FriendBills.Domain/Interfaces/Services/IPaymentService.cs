﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IPaymentService : IGenericService<Payment>
    {
        IEnumerable<Payment> GetPaymentsByBill(long id);
        Bill UpdateBillSubtotal(Payment payment, Bill bill);
        Bill UpdadeIfPaid(Payment payment, Bill billToUpdate);
    }
}
