﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IPersonService : IGenericService<Person>
    {
        IEnumerable<Person> FindByName(string name);
        IEnumerable<Person> GetActivePersons();
        string GetFullName(Person p);
        void DesactivePerson(long id);
    }
}
