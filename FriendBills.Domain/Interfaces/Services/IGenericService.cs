﻿using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IGenericService<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(long id);

        void Insert(TEntity obj);
        void Update(TEntity obj);
        void Delete(long id);
        void Dispose();
    }

}
