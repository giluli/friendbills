﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IFriendGroupService : IGenericService<FriendGroup>
    {
        IEnumerable<FriendGroup> FindByName(string name);
        IEnumerable<FriendGroup> FindGroupsByPerson(long id);
    }
}
