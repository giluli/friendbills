﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IBillService : IGenericService<Bill>
    {
        IEnumerable<Bill> FindByDescription(string description);
        IEnumerable<Bill> GetPaidBills();
        IEnumerable<Bill> GetNotPaidBills();
        Bill GetByIdNoTracking(long id);
    }
}
