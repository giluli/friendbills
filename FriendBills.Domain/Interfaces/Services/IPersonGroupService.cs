﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Domain.Interfaces.Services
{
    public interface IPersonGroupService : IGenericService<PersonGroup>
    {
        IEnumerable<PersonGroup> GetPersonsByGroup(long id);
    }
}
