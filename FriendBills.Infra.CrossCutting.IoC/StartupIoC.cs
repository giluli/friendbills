﻿using FriendBills.Application.Services;
using FriendBills.Application.Interfaces;
using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Domain.Interfaces.Services;
using FriendBills.Domain.Services;
using FriendBills.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace FriendBills.Infra.CrossCutting.IoC
{
    public class StartupIoC
    {
        public static void RegisterIoC(IServiceCollection services)
        {
            //Repository Dependencies
            services.AddTransient<IBillRepository, BillRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IFriendGroupRepository, FriendGroupRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IPersonGroupRepository, PersonGroupRepository>();

            //Domain Services Dependencies
            services.AddTransient<IBillService, BillService>();
            services.AddTransient<IPersonService, PersonService>();
            services.AddTransient<IFriendGroupService, FriendGroupService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPersonGroupService, PersonGroupService>();

            //Application Services Dependencies
            services.AddTransient<IBillAppService, BillAppService>();
            services.AddTransient<IPersonAppService, PersonAppService>();
            services.AddTransient<IFriendGroupAppService, FriendGroupAppService>();
            services.AddTransient<IPaymentAppService, PaymentAppService>();
            services.AddTransient<IPersonGroupAppService, PersonGroupAppService>();
        }
    }
}
