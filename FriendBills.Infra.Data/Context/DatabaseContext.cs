﻿using FriendBills.Domain.Entities;
using FriendBills.Infra.Data.EntityConfig;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace FriendBills.Infra.Data.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(string connString) : base(connString)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<FriendGroup> Groups { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PersonGroup> PersonsGroups { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(50));

            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new BillConfiguration());
            modelBuilder.Configurations.Add(new FriendGroupConfiguration());
            modelBuilder.Configurations.Add(new PaymentConfiguration());
            //modelBuilder.Configurations.Add(new PersonGroupConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("StartDate") != null))
            {
                if(entry.State == EntityState.Added)
                {
                    entry.Property("StartDate").CurrentValue = DateTime.Now;
                }

                if(entry.State == EntityState.Modified)
                {
                    entry.Property("StartDate").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("IsActive") != null))
            {
                if(entry.State == EntityState.Added || entry.State == EntityState.Modified)
                {
                    entry.Property("IsActive").CurrentValue = true;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("IsPaid") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("IsPaid").CurrentValue = false;
                }
            }

            return base.SaveChanges();
        }
    }
}
