﻿using System.Data.Entity.Infrastructure;

namespace FriendBills.Infra.Data.Context
{
    public class DatabaseContextFactory : IDbContextFactory<DatabaseContext>
    {
        public DatabaseContext Create()
        {
            return new DatabaseContext("Server=(localdb)\\mssqllocaldb;Database=FriendBills;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}
