namespace FriendBills.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bill",
                c => new
                    {
                        BillId = c.Long(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100, unicode: false),
                        Date = c.DateTime(nullable: false),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsPaid = c.Boolean(nullable: false),
                        PersonId = c.Long(nullable: false),
                        FriendGroupId = c.Long(nullable: false),
                        FriendGroup_GroupId = c.Long(),
                        Person_PersonId = c.Long(),
                    })
                .PrimaryKey(t => t.BillId)
                .ForeignKey("dbo.FriendGroup", t => t.FriendGroup_GroupId)
                .ForeignKey("dbo.Person", t => t.Person_PersonId)
                .ForeignKey("dbo.FriendGroup", t => t.FriendGroupId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId)
                .Index(t => t.FriendGroupId)
                .Index(t => t.FriendGroup_GroupId)
                .Index(t => t.Person_PersonId);
            
            CreateTable(
                "dbo.FriendGroup",
                c => new
                    {
                        GroupId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                        PersonId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.GroupId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonId = c.Long(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 50, unicode: false),
                        Email = c.String(nullable: false, maxLength: 100, unicode: false),
                        NickName = c.String(nullable: false, maxLength: 20, unicode: false),
                        StartDate = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        PaymentId = c.Long(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BillId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.Bill", t => t.BillId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.BillId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.PersonGroup",
                c => new
                    {
                        PersonGroupId = c.Long(nullable: false, identity: true),
                        FriendGroupId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PersonGroupId)
                .ForeignKey("dbo.FriendGroup", t => t.FriendGroupId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.FriendGroupId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.PersonFriendGroup",
                c => new
                    {
                        Person_PersonId = c.Long(nullable: false),
                        FriendGroup_GroupId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_PersonId, t.FriendGroup_GroupId })
                .ForeignKey("dbo.Person", t => t.Person_PersonId)
                .ForeignKey("dbo.FriendGroup", t => t.FriendGroup_GroupId)
                .Index(t => t.Person_PersonId)
                .Index(t => t.FriendGroup_GroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bill", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Bill", "FriendGroupId", "dbo.FriendGroup");
            DropForeignKey("dbo.FriendGroup", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonGroup", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonGroup", "FriendGroupId", "dbo.FriendGroup");
            DropForeignKey("dbo.Payment", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Payment", "BillId", "dbo.Bill");
            DropForeignKey("dbo.PersonFriendGroup", "FriendGroup_GroupId", "dbo.FriendGroup");
            DropForeignKey("dbo.PersonFriendGroup", "Person_PersonId", "dbo.Person");
            DropForeignKey("dbo.Bill", "Person_PersonId", "dbo.Person");
            DropForeignKey("dbo.Bill", "FriendGroup_GroupId", "dbo.FriendGroup");
            DropIndex("dbo.PersonFriendGroup", new[] { "FriendGroup_GroupId" });
            DropIndex("dbo.PersonFriendGroup", new[] { "Person_PersonId" });
            DropIndex("dbo.PersonGroup", new[] { "PersonId" });
            DropIndex("dbo.PersonGroup", new[] { "FriendGroupId" });
            DropIndex("dbo.Payment", new[] { "PersonId" });
            DropIndex("dbo.Payment", new[] { "BillId" });
            DropIndex("dbo.FriendGroup", new[] { "PersonId" });
            DropIndex("dbo.Bill", new[] { "Person_PersonId" });
            DropIndex("dbo.Bill", new[] { "FriendGroup_GroupId" });
            DropIndex("dbo.Bill", new[] { "FriendGroupId" });
            DropIndex("dbo.Bill", new[] { "PersonId" });
            DropTable("dbo.PersonFriendGroup");
            DropTable("dbo.PersonGroup");
            DropTable("dbo.Payment");
            DropTable("dbo.Person");
            DropTable("dbo.FriendGroup");
            DropTable("dbo.Bill");
        }
    }
}
