﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;

namespace FriendBills.Infra.Data.Repositories
{
    public class PaymentRepository : GenericRepository<Payment>, IPaymentRepository
    {
    }
}
