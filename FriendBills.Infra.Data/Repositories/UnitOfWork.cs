﻿using FriendBills.Domain.Entities;
using FriendBills.Infra.Data.Context;
using System;

namespace FriendBills.Infra.Data.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private DatabaseContext context;
        private GenericRepository<Person> personRepository;
        private GenericRepository<Bill> billDetailRepository;

        private bool disposed = false;

        public GenericRepository<Person> PersonRepository()
        {
            if (this.personRepository == null)
            {
                this.personRepository = new GenericRepository<Person>(context);
            }
            return personRepository;
        }

        public GenericRepository<Bill> BillDetailRepository()
        {
            if (this.billDetailRepository == null)
            {
                this.billDetailRepository = new GenericRepository<Bill>(context);
            }
            return billDetailRepository;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
