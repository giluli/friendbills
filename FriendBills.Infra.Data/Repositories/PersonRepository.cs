﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;
using System;

namespace FriendBills.Infra.Data.Repositories
{
    public class PersonRepository : GenericRepository<Person>, IPersonRepository
    { 
        public IEnumerable<Person> FindByName(string name)
        {
            return _context.Persons
                .Where(p => p.FirstName.Equals(name, StringComparison.InvariantCultureIgnoreCase) 
                    || p.LastName.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public void DesactivePerson(long id)
        {
            Person entityToDesactive = GetById(id);
            entityToDesactive.IsActive = false;
            Update(entityToDesactive);
        }
    }
}
