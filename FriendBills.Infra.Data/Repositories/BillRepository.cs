﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Infra.Data.Repositories
{
    public class BillRepository : GenericRepository<Bill>, IBillRepository
    {
        public IEnumerable<Bill> FindByDescription(string description)
        {
            return _context.Bills
                .Where(b => b.Description.Equals(description, StringComparison.InvariantCultureIgnoreCase));
        }

        public Bill GetByIdNoTracking(long id)
        {
            var entity = dbSet.Find(id);
            Detach(entity);
            return entity;
        }
    }
}
