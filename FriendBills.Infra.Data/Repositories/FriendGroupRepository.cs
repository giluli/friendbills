﻿using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FriendBills.Infra.Data.Repositories
{
    public class FriendGroupRepository : GenericRepository<FriendGroup>, IFriendGroupRepository
    {
        public IEnumerable<FriendGroup> FindByName(string name)
        {
            return _context.Groups.Where(g => g.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
