﻿using FriendBills.Domain.Interfaces.Repositories;
using FriendBills.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FriendBills.Infra.Data.Repositories
{
    public class GenericRepository<TEntity> : IDisposable, IGenericRepository<TEntity> where TEntity : class
    {
        protected DatabaseContext _context;
        protected DbSet<TEntity> dbSet;
        protected bool disposed = false;

        public GenericRepository()
        {
            if (this._context == null)
            {
                this._context = new DatabaseContext("Server=(localdb)\\mssqllocaldb;Database=FriendBills;Trusted_Connection=True;MultipleActiveResultSets=true");
                this.dbSet = _context.Set<TEntity>();
            }
        }

        public GenericRepository(DatabaseContext context)
        {
            this._context = context;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return dbSet.ToList();
        }

        public TEntity GetById(long id)
        { 
            var entity = dbSet.Find(id);
            return entity;
        }

        public void Insert(TEntity entity)
        {
            dbSet.Add(entity);
            _context.Entry(entity).State = EntityState.Added;
            Save();
        }

        public void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            Save();   
        }

        public void Delete(long id)
        {
            TEntity entityToDelete = GetById(id);
            Delete(entityToDelete);
            Save();
        }

        protected void Detach(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }

        protected void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
                dbSet.Remove(entity);
        }

        protected void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        { 
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
