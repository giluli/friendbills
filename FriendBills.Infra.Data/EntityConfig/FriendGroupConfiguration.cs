﻿using FriendBills.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FriendBills.Infra.Data.EntityConfig
{
    public class FriendGroupConfiguration : EntityTypeConfiguration<FriendGroup>
    {
        public FriendGroupConfiguration()
        {
            HasKey(g =>  g.FriendGroupId);
            Property(g => g.Name).IsRequired().HasMaxLength(100);
            HasRequired(g => g.Person).WithMany().HasForeignKey(g => g.PersonId);
        }
    }
}
