﻿using FriendBills.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FriendBills.Infra.Data.EntityConfig
{
    public class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            HasKey(p => p.PersonId);
            Property(p => p.FirstName).IsRequired().HasMaxLength(50);
            Property(p => p.LastName).IsRequired().HasMaxLength(50);
            Property(p => p.NickName).IsRequired().HasMaxLength(20);
            Property(p => p.Email).IsRequired().HasMaxLength(100);
            Property(p => p.StartDate).IsRequired();
        }
    }
}
