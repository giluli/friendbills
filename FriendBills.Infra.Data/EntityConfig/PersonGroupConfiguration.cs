﻿using FriendBills.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FriendBills.Infra.Data.EntityConfig
{
    public class PersonGroupConfiguration : EntityTypeConfiguration<PersonGroup>
    {
        public PersonGroupConfiguration()
        {
            HasKey(p => new { p.FriendGroupId, p.PersonId });
 
        }
    }
}
