﻿using FriendBills.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FriendBills.Infra.Data.EntityConfig
{
    public class BillConfiguration : EntityTypeConfiguration<Bill>
    {
        public BillConfiguration()
        {
            HasKey(b => b.BillId);
            Property(b => b.Description).IsRequired().HasMaxLength(100);
            Property(b => b.Date).IsRequired();
            Property(b => b.Total).IsRequired();
            HasRequired(b => b.Person).WithMany().HasForeignKey(b => b.PersonId);
            HasRequired(b => b.FriendGroup).WithMany().HasForeignKey(b => b.FriendGroupId);
        }
    }
}
