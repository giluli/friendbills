﻿using FriendBills.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FriendBills.Infra.Data.EntityConfig
{
    public class PaymentConfiguration : EntityTypeConfiguration<Payment>
    {
        public PaymentConfiguration()
        {
            HasKey(p => p.PaymentId)
                .ToTable("Payment");
            Property(p => p.Amount).IsRequired();
            Property(p => p.Date).IsRequired();
            HasRequired(p => p.Bill)
                .WithMany(p => p.Payments)
                .HasForeignKey(p => p.BillId);
            HasRequired(p => p.Person)
                .WithMany(p => p.Payments)
                .HasForeignKey(p => p.PersonId);
        }
    }
}
