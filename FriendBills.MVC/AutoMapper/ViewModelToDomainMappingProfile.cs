﻿using AutoMapper;
using FriendBills.Domain.Entities;
using FriendBills.Presentation.ViewModels;

namespace FriendBills.Presentation.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<Bill, BillViewModel>();
            CreateMap<Person, PersonViewModel>();
            CreateMap<FriendGroup, FriendGroupViewModel>();
            CreateMap<Payment, PaymentViewModel>();
            CreateMap<PersonGroup, PersonGroupViewModel>();
        }
    }
}
