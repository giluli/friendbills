﻿using AutoMapper;
using FriendBills.Domain.Entities;
using FriendBills.Presentation.ViewModels;

namespace FriendBills.Presentation.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelMappings";

        public DomainToViewModelMappingProfile()
        {
            CreateMap<BillViewModel, Bill>();
            CreateMap<PersonViewModel, Person>();
            CreateMap<FriendGroupViewModel, FriendGroup>();
            CreateMap<PaymentViewModel, Payment>();
            CreateMap<PersonGroupViewModel, PersonGroup>();
        }

    }
}
