﻿using System.ComponentModel.DataAnnotations;

namespace FriendBills.Presentation.ViewModels
{
    public class PersonGroupViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long PersonGroupId { get; set; }

        [Key]
        [Required(ErrorMessage = "Selecione o grupo")]
        public long FriendGroupId { get; set; }

        [Key]
        [Required(ErrorMessage = "Selecione o usuário")]
        public long PersonId { get; set; }

        public virtual FriendGroupViewModel FriendGroup { get; set; }
        public virtual PersonViewModel Person { get; set; }
    }
}
