﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FriendBills.Presentation.ViewModels
{
    public class PaymentViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long PaymentId { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Informe a Data")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Data do pagamento")]
        public DateTime Date { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0", "999999999999")]
        public decimal Amount { get; set; }

        [Key]
        [ScaffoldColumn(false)]
        public long BillId { get; set; }

        [Key]
        [ScaffoldColumn(false)]
        public long PersonId { get; set; }

        public virtual BillViewModel Bill { get; set; }
        public virtual PersonViewModel Person { get; set; }
    }
}
