using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FriendBills.Application.Interfaces;
using FriendBills.Presentation.AutoMapper;
using FriendBills.Presentation.ViewModels;
using FriendBills.Domain.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FriendBills.MVC.Controllers
{
    public class GroupController : Controller
    {
        private readonly IFriendGroupAppService _groupApp;
        private readonly IPersonAppService _personApp;
        private readonly IPersonGroupAppService _personGroupApp;


        public GroupController(IFriendGroupAppService groupApp, IPersonAppService personApp, IPersonGroupAppService personGroup)
        {
            _groupApp = groupApp;
            _personApp = personApp;
            _personGroupApp = personGroup;
        }

        // GET: Group
        public IActionResult Index()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<FriendGroupViewModel>, IEnumerable<FriendGroup>>(_groupApp.GetAll()));
        }

        // GET: Group/Details/5
        public IActionResult Details(long id)
        {
            var groupViewModel = GetDetailsByGroup(id);
            groupViewModel.PersonsGroup = new PersonGroupController(_personGroupApp, _personApp,  _groupApp).GetPersonsByGroup(id);

            return View(groupViewModel);
        }

        // GET: Group/Create
        public IActionResult Create()
        {
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
            return View();
        }

        // POST: Group/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FriendGroupViewModel currentGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _groupApp.Insert(AutoMapperConfig.MapViewModelToDomain<FriendGroup, FriendGroupViewModel>(currentGroup));

                    return RedirectToAction("Index");
                }

                ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");

                return View(currentGroup);
            }
            catch
            {
                return View();
            }
        }

        // GET: Group/Edit/5
        public IActionResult Edit(long id)
        {
            var groupViewModel = GetDetailsByGroup(id);
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName", groupViewModel.PersonId);

            return View(groupViewModel);
        }

        // POST: Group/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FriendGroupViewModel currentGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _groupApp.Update(AutoMapperConfig.MapViewModelToDomain<FriendGroup, FriendGroupViewModel>(currentGroup));

                    return RedirectToAction("Index");
                }

                return View(currentGroup);
            }
            catch
            {
                return View();
            }
        }

        // GET: Group/Delete/5
        public IActionResult Delete(long id)
        {
            var groupViewModel = GetDetailsByGroup(id);

            return View(groupViewModel);
        }

        // POST: Group/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            try
            {
                _groupApp.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //private methods
        private FriendGroupViewModel GetDetailsByGroup(long id)
        {
            var group = _groupApp.GetById(id);
            return AutoMapperConfig.MapDomainToViewModel<FriendGroupViewModel, FriendGroup>(group);
        }
    }
}