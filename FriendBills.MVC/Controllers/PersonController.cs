using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Presentation.ViewModels;
using FriendBills.Presentation.AutoMapper;

namespace FriendBills.MVC.Controllers
{
    public class PersonController : Controller
    {
        private readonly IPersonAppService _personApp;

        public PersonController(IPersonAppService personApp)
        {
            _personApp = personApp;
        }

        // GET: /Person/
        public IActionResult Index()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<PersonViewModel>, IEnumerable<Person>>(_personApp.GetAll()));
        }

        public IActionResult GetActivePersons()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<PersonViewModel>, IEnumerable<Person>>(_personApp.GetActivePersons()));
        }

        // GET: /Person/Details/?
        public IActionResult Details(long id)
        {
            var personViewModel = GetDetailsByPerson(id);

            return View(personViewModel);
        }

        // GET: /Person/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: /Person/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PersonViewModel currentPerson)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _personApp.Insert(AutoMapperConfig.MapViewModelToDomain<Person, PersonViewModel>(currentPerson));
                    return RedirectToAction("Index");
                }

                return View(currentPerson);
            }
            catch
            {
                return View();
            }
        }

        // GET: /Person/Edit/?
        public IActionResult Edit(long id)
        {
            var personViewModel = GetDetailsByPerson(id);

            return View(personViewModel);
        }

        // POST: /Person/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PersonViewModel currentPerson)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _personApp.Update(AutoMapperConfig.MapViewModelToDomain<Person, PersonViewModel>(currentPerson));
                    return RedirectToAction("Index");
                }

                return View(currentPerson);
            }
            catch
            {
                return View();
            }
        }

        // GET: /Person/Delete/?
        public IActionResult Delete(long id)
        {
            var personViewModel = GetDetailsByPerson(id);

            return View(personViewModel);
        }

        // POST: /Person/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            try
            {
                _personApp.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //private methods
        private PersonViewModel GetDetailsByPerson(long id)
        {
            var person = _personApp.GetById(id);
            return AutoMapperConfig.MapDomainToViewModel<PersonViewModel, Person>(person);

        }
    }
}