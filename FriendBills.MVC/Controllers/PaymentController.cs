using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FriendBills.Presentation.ViewModels;
using FriendBills.Domain.Entities;
using FriendBills.Application.Interfaces;
using FriendBills.Presentation.AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace FriendBills.MVC.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IPaymentAppService _paymentApp;
        private readonly IBillAppService _billApp;
        private readonly IPersonAppService _personApp;
        private readonly IFriendGroupAppService _groupApp;

        public PaymentController(IBillAppService billApp, IPersonAppService personApp, IPaymentAppService paymentApp, IFriendGroupAppService groupApp)
        {
            _billApp = billApp;
            _personApp = personApp;
            _paymentApp = paymentApp;
            _groupApp = groupApp;
        }

        // GET: Payment
        public IActionResult Index()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<PaymentViewModel>, IEnumerable<Payment>>(_paymentApp.GetAll()));
        }

        // GET: Payment/Teste
        public IActionResult Create(long id)
        {
            ViewBag.BillId = new SelectList(_billApp.GetNotPaidBills(), "BillId", "Description", id);
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
            ViewBag.BillId_Detail = id;
            return View();
        }

        // POST: Payment/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PaymentViewModel currentPayment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BillController bc = new BillController(_billApp, _personApp, _paymentApp, _groupApp);

                    _paymentApp.Insert(AutoMapperConfig.MapViewModelToDomain<Payment, PaymentViewModel>(currentPayment));

                    Bill billToUpdate = _paymentApp.UpdateBillSubtotal(
                        AutoMapperConfig.MapViewModelToDomain<Payment, PaymentViewModel>(currentPayment), 
                        AutoMapperConfig.MapViewModelToDomain<Bill, BillViewModel>(bc.GetDetailsByBill(currentPayment.BillId, false)));

                    billToUpdate = _paymentApp.UpdadeIfPaid(AutoMapperConfig.MapViewModelToDomain<Payment, PaymentViewModel>(currentPayment), billToUpdate);

                    _billApp.Update(billToUpdate);
                }

                ViewBag.BillId = new SelectList(_billApp.GetNotPaidBills(), "BillId", "Description", currentPayment.BillId);
                ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");

                return RedirectToAction("Details", "Bill", new { id = currentPayment.BillId });
            }
            catch
            {
                return View();
            }
        }

        // GET: Payment/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: Payment/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public IEnumerable<PaymentViewModel> GetPaymentsByBill(long id)
        {
            return AutoMapperConfig.MapDomainToViewModel<IEnumerable<PaymentViewModel>, IEnumerable<Payment>>(_paymentApp.GetPaymentsByBill(id));
        }

    }
}