using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FriendBills.Application.Interfaces;
using FriendBills.Presentation.ViewModels;
using FriendBills.Domain.Entities;
using FriendBills.Presentation.AutoMapper;

namespace FriendBills.MVC.Controllers
{
    public class PersonGroupController : Controller
    {
        private readonly IPersonGroupAppService _personGroupApp;
        private readonly IPersonAppService _personApp;
        private readonly IFriendGroupAppService _groupApp;
        
        public PersonGroupController(IPersonGroupAppService personGroupApp, IPersonAppService personApp, IFriendGroupAppService groupApp)
        {
            _personGroupApp = personGroupApp;
            _personApp = personApp;
            _groupApp = groupApp;
        }


        // GET: PersonGroup
        public IActionResult Index()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<PersonGroupViewModel>, IEnumerable<PersonGroup>>(_personGroupApp.GetAll()));
        }

        // GET: PersonGroup/Create
        public IActionResult Create(long id)
        {
            ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "FriendGroupId", "Name", id);
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
            ViewBag.GroupId_Detail = id;

            return View();
        }

        // POST: PersonGroup/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PersonGroupViewModel currentPersonGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _personGroupApp.Insert(AutoMapperConfig.MapViewModelToDomain<PersonGroup, PersonGroupViewModel>(currentPersonGroup));
                }

                ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
                ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "FriendGroupId", "Name");

                return RedirectToAction("Details", "Group", new { id = currentPersonGroup.FriendGroupId } );
            }
            catch
            {
                return View();
            }
        }

        
        // GET: PersonGroup/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PersonGroup/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public IEnumerable<PersonGroupViewModel> GetPersonsByGroup(long id)
        {
            return AutoMapperConfig.MapDomainToViewModel<IEnumerable<PersonGroupViewModel>, IEnumerable<PersonGroup>>(_personGroupApp.GetPersonsByGroup(id));
        }
    }
}