using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using FriendBills.Presentation.AutoMapper;
using System;

namespace FriendBills.MVC.Controllers
{
    public class BillController : Controller
    {
        private readonly IBillAppService _billApp;
        private readonly IPersonAppService _personApp;
        private readonly IPaymentAppService _paymentApp;
        private readonly IFriendGroupAppService _groupApp;

        public BillController(IBillAppService billApp, IPersonAppService personApp, IPaymentAppService paymentApp, IFriendGroupAppService groupApp)
        {
            _billApp = billApp;
            _personApp = personApp;
            _paymentApp = paymentApp;
            _groupApp = groupApp;
        }

        // GET: /Bill/
        public IActionResult Index()
        {
            return View(AutoMapperConfig.MapDomainToViewModel<IEnumerable<BillViewModel>, IEnumerable<Bill>>(_billApp.GetAll()));
        }

        // GET: /Bill/
        public IActionResult GetPaidBills()
        {
            return View("Index", AutoMapperConfig.MapDomainToViewModel<IEnumerable<BillViewModel>, IEnumerable<Bill>>(_billApp.GetPaidBills()));
        }

        // GET: /Bill/
        public IActionResult GetNotPaidBills()
        {
            return View("Index", AutoMapperConfig.MapDomainToViewModel<IEnumerable<BillViewModel>, IEnumerable<Bill>>(_billApp.GetNotPaidBills()));
        }

        // GET: /Bill/Details/?
        public IActionResult Details(long id)
        {
            var billViewModel = GetDetailsByBill(id, true);
            billViewModel.Payments = new PaymentController(_billApp, _personApp, _paymentApp, _groupApp).GetPaymentsByBill(id); 
            return View(billViewModel);
        }

        // GET: /Bill/Create
        public IActionResult Create()
        {
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
            ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "FriendGroupId", "Name");

            return View();
        }

        // POST: /Bill/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BillViewModel currentBill)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _billApp.Insert(AutoMapperConfig.MapViewModelToDomain<Bill, BillViewModel>(currentBill));

                    return RedirectToAction("Index");
                }

                ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName");
                ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "FriendGroupId", "Name");

                return View(currentBill);
            }
            catch
            {
                return View();
            }
        }

        // GET: /Bill/Edit/?
        public IActionResult Edit(long id)
        {
            var billViewModel = GetDetailsByBill(id, true);
            ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName", billViewModel.PersonId);
            ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "FriendGroupId", "Name", billViewModel.FriendGroupId);

            return View(billViewModel);
        }

        // POST: /Bill/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BillViewModel currentBill)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    Payment payment = new Payment();
                        payment.Amount = currentBill.SubTotal;

                    Bill billToUpdate = _paymentApp.UpdadeIfPaid(payment, AutoMapperConfig.MapViewModelToDomain<Bill, BillViewModel>(currentBill));
                    _billApp.Update(billToUpdate);

                    return RedirectToAction("Index");
                }

                ViewBag.PersonId = new SelectList(_personApp.GetActivePersons(), "PersonId", "NickName", currentBill.PersonId);
                ViewBag.FriendGroupId = new SelectList(_groupApp.GetAll(), "GroupId", "Name", currentBill.FriendGroupId);

                return View(currentBill);
            }
            catch
            {
                return View();
            }     
        }

        // GET: /Bill/Delete/?
        public IActionResult Delete(long id)
        {
            var billViewModel = GetDetailsByBill(id, true);

            return View(billViewModel);
        }

        // POST: /Bill/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            try
            {
                _personApp.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public BillViewModel GetDetailsByBill(long id, bool tracking)
        {
            Bill bill = new Bill();
            if(tracking)
            {
                bill = _billApp.GetById(id);
            }
            else
            {
                bill = _billApp.GetByIdNoTracking(id);
            }
            return AutoMapperConfig.MapDomainToViewModel<BillViewModel, Bill>(bill);
        }
    }
}