﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Application.Interfaces
{
    public interface IBillAppService : IGenericAppService<Bill>
    {
        IEnumerable<Bill> FindByDescription(string description);
        IEnumerable<Bill> GetPaidBills();
        IEnumerable<Bill> GetNotPaidBills();
        Bill GetByIdNoTracking(long id);
    }
}
