﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Application.Interfaces
{
    public interface IPersonGroupAppService : IGenericAppService<PersonGroup>
    {
        IEnumerable<PersonGroup> GetPersonsByGroup(long id);
    }
}
