﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Application.Interfaces
{
    public interface IPaymentAppService : IGenericAppService<Payment>
    {
        IEnumerable<Payment> GetPaymentsByBill(long id);
        Bill UpdateBillSubtotal(Payment payment, Bill bill);
        Bill UpdadeIfPaid(Payment payment, Bill billToUpdate);
    }
}
