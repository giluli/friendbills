﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Application.Interfaces
{
    public interface IPersonAppService : IGenericAppService<Person>
    {
        IEnumerable<Person> FindByName(string name);
        IEnumerable<Person> GetActivePersons();
        string GetFullName(Person p);
        void DesactivePerson(long id);
    }
}
