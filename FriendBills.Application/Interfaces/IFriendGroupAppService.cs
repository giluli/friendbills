﻿using FriendBills.Domain.Entities;
using System.Collections.Generic;

namespace FriendBills.Application.Interfaces
{
    public interface IFriendGroupAppService : IGenericAppService<FriendGroup>
    {
        IEnumerable<FriendGroup> FindByName(string name);
        IEnumerable<FriendGroup> FindGroupsByPerson(long id);
    }
}
