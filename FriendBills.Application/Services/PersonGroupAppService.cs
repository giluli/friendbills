﻿using System;
using System.Collections.Generic;
using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Services;

namespace FriendBills.Application.Services
{
    public class PersonGroupAppService : GenericAppService<PersonGroup>, IPersonGroupAppService
    {
        private readonly IPersonGroupService _personGroupService;

        public PersonGroupAppService(IPersonGroupService personGroupService) : base(personGroupService)
        {
            _personGroupService = personGroupService;
        }

        public IEnumerable<PersonGroup> GetPersonsByGroup(long id)
        {
            return _personGroupService.GetPersonsByGroup(id);
        }
    }
}
