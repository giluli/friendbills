﻿using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace FriendBills.Application.Services
{
    public class BillAppService : GenericAppService<Bill>, IBillAppService
    {
        private readonly IBillService _billService;

        public BillAppService(IBillService billService) : base(billService)
        {
            _billService = billService;
        }

        public IEnumerable<Bill> FindByDescription(string description)
        {
            return _billService.FindByDescription(description);
        }

        public IEnumerable<Bill> GetNotPaidBills()
        {
            return _billService.GetNotPaidBills();
        }

        public IEnumerable<Bill> GetPaidBills()
        {
            return _billService.GetPaidBills();
        }

        public Bill GetByIdNoTracking(long id)
        {
            return _billService.GetByIdNoTracking(id);
        }
    }
}
