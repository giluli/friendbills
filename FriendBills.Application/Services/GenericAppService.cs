﻿using FriendBills.Application.Interfaces;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;
using System;

namespace FriendBills.Application.Services
{
    public class GenericAppService<TEntity> : IGenericAppService<TEntity> where TEntity : class
    {
        private readonly IGenericService<TEntity> _genericService;

        public GenericAppService(IGenericService<TEntity> genericService)
        {
            _genericService = genericService;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _genericService.GetAll();
        }

        public TEntity GetById(long id)
        {
            return _genericService.GetById(id);
        }

        public void Insert(TEntity entity)
        {
            _genericService.Insert(entity);
        }

        public void Update(TEntity entity)
        {
            _genericService.Update(entity);
        }

        public void Delete(long id)
        {
            _genericService.Delete(id);
        }

        public void Dispose()
        {
            _genericService.Dispose();
        }
    }
}
