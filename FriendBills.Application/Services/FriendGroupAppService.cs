﻿using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace FriendBills.Application.Services
{
    public class FriendGroupAppService : GenericAppService<FriendGroup>, IFriendGroupAppService
    {
        private readonly IFriendGroupService _friendGroupService;

        public FriendGroupAppService(IFriendGroupService friendGroupService) : base(friendGroupService)
        {
            _friendGroupService = friendGroupService;
        }

        public IEnumerable<FriendGroup> FindByName(string name)
        {
            return _friendGroupService.FindByName(name);
        }

        public IEnumerable<FriendGroup> FindGroupsByPerson(long id)
        {
            return _friendGroupService.FindGroupsByPerson(id);
        }
    }
}
