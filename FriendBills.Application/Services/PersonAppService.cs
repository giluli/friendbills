﻿using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace FriendBills.Application.Services
{
    public class PersonAppService : GenericAppService<Person>, IPersonAppService
    {
        private readonly IPersonService _personService;

        public PersonAppService(IPersonService personService) : base(personService)
        {
            _personService = personService;
        }

        public IEnumerable<Person> FindByName(string name)
        {
            return _personService.FindByName(name);
        }

        public IEnumerable<Person> GetActivePersons()
        {
            return _personService.GetAll();
        }

        public string GetFullName(Person p)
        {
            return _personService.GetFullName(p);
        }


        public void DesactivePerson(long id)
        {
            _personService.DesactivePerson(id);
        }
    }
}
