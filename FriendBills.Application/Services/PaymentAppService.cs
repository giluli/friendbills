﻿using System.Collections.Generic;
using FriendBills.Application.Interfaces;
using FriendBills.Domain.Entities;
using FriendBills.Domain.Interfaces.Services;

namespace FriendBills.Application.Services
{
    public class PaymentAppService : GenericAppService<Payment>, IPaymentAppService
    {
        private readonly IPaymentService _paymentService;

        public PaymentAppService(IPaymentService paymentService) : base(paymentService)
        {
            _paymentService = paymentService;
        }

        public IEnumerable<Payment> GetPaymentsByBill(long id)
        {
            return _paymentService.GetPaymentsByBill(id);
        }

        public Bill UpdateBillSubtotal(Payment payment, Bill billToUpdate)
        {
            return _paymentService.UpdateBillSubtotal(payment, billToUpdate);
        }

        public Bill UpdadeIfPaid(Payment payment, Bill billToUpdate)
        {
            return _paymentService.UpdadeIfPaid(payment, billToUpdate);
        }
    }
}
