﻿using AutoMapper;
using System.Collections.Generic;

namespace FriendBills.Application.AutoMapper
{
    public class AutoMapperConfig
    {

        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();
            });
        }

        public static TEntity MapViewModelToDomain<TEntity, TEntityModelView>(TEntityModelView viewModel)
            where TEntity : class where TEntityModelView : class
        {
            return Mapper.Map<TEntityModelView, TEntity>(viewModel);
        }

        public static IEnumerable<TEntity> MapViewModelToDomain<TEntity, TEntityModelView>(IEnumerable<TEntityModelView> viewModel)
             where TEntityModelView : IEnumerable<TEntityModelView> where TEntity : IEnumerable<TEntity>
        {
            return Mapper.Map<IEnumerable<TEntityModelView>, IEnumerable<TEntity>>(viewModel);
        }

        public static TEntityModelView MapDomainToViewModel<TEntityModelView, TEntity>(TEntity entity)
           where TEntityModelView : class where TEntity : class
        {
            return Mapper.Map<TEntity, TEntityModelView>(entity);
        }

        public static IEnumerable<TEntityModelView> MapDomainToViewModel<TEntityModelView, TEntity>(IEnumerable<TEntity> entity)
           where TEntityModelView : IEnumerable<TEntityModelView> where TEntity : IEnumerable<TEntity>
        {
            return Mapper.Map<IEnumerable<TEntity>, IEnumerable<TEntityModelView>>(entity);
        }
    }
}
