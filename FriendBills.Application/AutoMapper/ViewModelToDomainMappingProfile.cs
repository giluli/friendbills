﻿using AutoMapper;
using FriendBills.Domain.Entities;
using FriendBills.Application.ViewModels;

namespace FriendBills.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelMappings";

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<Bill, BillViewModel>();
            CreateMap<Person, PersonViewModel>();
            CreateMap<FriendGroup, FriendGroupViewModel>();
            CreateMap<Payment, PaymentViewModel>();
            CreateMap<PersonGroup, PersonGroupViewModel>();
        }
    }
}
