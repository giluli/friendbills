﻿using AutoMapper;
using FriendBills.Domain.Entities;
using FriendBills.Application.ViewModels;

namespace FriendBills.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public DomainToViewModelMappingProfile()
        {
            CreateMap<BillViewModel, Bill>();
            CreateMap<PersonViewModel, Person>();
            CreateMap<FriendGroupViewModel, FriendGroup>();
            CreateMap<PaymentViewModel, Payment>();
            CreateMap<PersonGroupViewModel, PersonGroup>();
        }

    }
}
