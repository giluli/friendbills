﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FriendBills.Application.ViewModels
{
    public class PersonGroupViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long PersonGroupId { get; set; }

        [Key]
        [Required(ErrorMessage = "Selecione o grupo")]
        public long FriendGroupId { get; set; }

        [Key]
        [Required(ErrorMessage = "Selecione o usuário")]
        public long PersonId { get; set; }

        public virtual FriendGroupViewModel FriendGroup { get; set; }
        public virtual PersonViewModel Person { get; set; }
    }
}
