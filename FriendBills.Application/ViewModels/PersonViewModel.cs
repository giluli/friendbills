﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FriendBills.Application.ViewModels
{
    public class PersonViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long PersonId { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(50, ErrorMessage ="Máximo de {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo de {0} caracteres")]
        [Display(Name = "Nome")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(50, ErrorMessage = "Máximo de {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo de {0} caracteres")]
        [Display(Name = "Sobrenome")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Preencha o campo E-mail")]
        [MaxLength(100, ErrorMessage = "Máximo de {0} caracteres")]
        [EmailAddress(ErrorMessage = "Preencha um E-mail válido")]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Preencha o campo Apelido")]
        [MaxLength(20, ErrorMessage = "Máximo de {0} caracteres")]
        [DisplayName("Apelido")]
        public string NickName { get; set; }

        [ScaffoldColumn(false)]
        public DateTime StartDate { get; set; }

        [ScaffoldColumn(false)]
        public bool IsActive { get; set; }

        public virtual IEnumerable<BillViewModel> Bills { get; set; }
    }
}
