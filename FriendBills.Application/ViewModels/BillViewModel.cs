﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FriendBills.Application.ViewModels
{
    public class BillViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long BillId { get; set; }

        [Required(ErrorMessage = "Preencha o campo Descrição")]
        [MaxLength(100, ErrorMessage = "Máximo de {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo de {0} caracteres")]
        [DisplayName("Descrição")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Informe a Data")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Data da conta")]
        public DateTime Date { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0", "999999999999")]
        public decimal SubTotal { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0", "999999999999")]
        [Required(ErrorMessage = "Informe o valor da conta")]
        public decimal Total { get; set; }

        public bool IsPaid { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "Selecione o usuário proprietário da conta")]
        public long PersonId { get; set; }

        public virtual PersonViewModel Person { get; set; }
    }
}
