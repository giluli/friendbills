﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FriendBills.Application.ViewModels
{
    public class FriendGroupViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public long FriendGroupId { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(100, ErrorMessage = "Máximo de {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo de {0} caracteres")]
        [DisplayName("Nome")]
        public string Name { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "Selecione o usuário proprietário da conta")]
        public long PersonId { get; set; }

        public virtual PersonViewModel Person { get; set; }
    }
}
