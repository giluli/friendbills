# What is the FrindBills Project? #

These are currently an early work in progress. 
This is a test for me, because is a new programming language. But it works like a test for Stone people too. I chose the challenge 2.

### Technologies implemented ###
* Visual Studio 2017
* SQL Server
* EntityFramework 6.1.3
* AspNetCore 1.1.2
* AspNetCoreMVC 1.1.3
* AutoMapper 6.1.1
* AutoMapper.Extensios.Microsoft.DependencyInjection 2.0.1

### This is BETA ###
* This project have a lot of issues and i will work as soon as possible to better and fix the bugs.
* Version Release 0.1

### Patterns Applied ###
* DDD (Domain Driven Design)
* Dependency Injection
* Invertion of Control
* View Model

### Contribution guidelines ###
* Stackoverflow :)
* Youtube

### About the next versions ###
* Create a REST Web API (presentation layer calls Web API)
* Unit testing of C# classes using NUnit
* Validation Exceptions - Ex:DbEntityValidationException, Exception
* Website Personalization
* Authentication